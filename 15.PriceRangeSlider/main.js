// import RangeSliderPips from "https://cdn.skypack.dev/svelte-range-slider-pips@2.2.2";

let values = [3000, 7000];
let timer;

const customSlider = document.getElementById("custom-slider");

const currencyFormatter = new Intl.NumberFormat("en", {
  style: "currency",
  currency: "USD",
  maximumFractionDigits: 0,
});

const formatCurrency = (value) => currencyFormatter.format(value);

const stopSlider = () => {
  const slider = document.querySelector("#CustomGradientSlider");
  slider.classList.remove("up", "down");
};

const slideHandler = (event) => {
  const slider = document.querySelector("#CustomGradientSlider");
  const delta = -(event.detail.previousValue - event.detail.value);
  if (delta > 0) {
    slider.classList.add("up");
    slider.classList.remove("down");
  } else {
    slider.classList.add("down");
    slider.classList.remove("up");
  }
  clearTimeout(timer);
  timer = setTimeout(stopSlider, 66);
};

const customRangeSlider = new RangeSliderPips({
  target: customSlider,
  props: {
    id: "CustomGradientSlider",
    min: 0,
    max: 10000,
    values: values,
    pips: true,
    range: true,
    pipstep: 200,
    first: false,
    last: false,
    float: true,
    formatter: formatCurrency,
  },
});

customRangeSlider.$on("change", slideHandler);
customRangeSlider.$on("stop", stopSlider);

setTimeout(() => {
  document.querySelector("#CustomGradientSlider .rangeHandle").focus();
}, 1000);