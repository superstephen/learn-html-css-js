前端小项目合集

1.[图片切换效果](https://gitee.com/superstephen/learn-html-css-js/tree/master/1.AvatarHoverEffect)

2.[拟物风格响应式布局的个人展示页面](https://gitee.com/superstephen/learn-html-css-js/tree/master/2.ResponsiveProfileWebsite)

3.[图片切换效果](https://gitee.com/superstephen/learn-html-css-js/tree/master/3.ImageHorizenHover)

4.[炫酷登录窗口](https://gitee.com/superstephen/learn-html-css-js/tree/master/4.AnimatedLoginFormWithLoading)

5.[搜索框动画](https://gitee.com/superstephen/learn-html-css-js/tree/master/5.AnimatedSearchBar)

6.[图片轮播](https://gitee.com/superstephen/learn-html-css-js/tree/master/6.BackgroundImageSlide)

7.[圣诞快乐](https://gitee.com/superstephen/learn-html-css-js/tree/master/7.MerryChristmas)

8.[磨砂玻璃登录页面](https://gitee.com/superstephen/learn-html-css-js/tree/master/8.GlassmorphismLoginForm)

9.[新年快乐2024](https://gitee.com/superstephen/learn-html-css-js/tree/master/9.HappyNewYear2024)

10.[可口可乐卡片动画](https://gitee.com/superstephen/learn-html-css-js/tree/master/10.CocolaCard)

11.[购物车按钮动画](https://gitee.com/superstephen/learn-html-css-js/tree/master/10.OrderButton)

12.[3D汽车动画](https://gitee.com/superstephen/learn-html-css-js/tree/master/12.3DCarLandingPage)

13.[响应式透明任务介绍卡片](https://gitee.com/superstephen/learn-html-css-js/tree/master/13.glassmorphism-card-responsive)

14.[进度条范围](https://gitee.com/superstephen/learn-html-css-js/tree/master/14.range-slider)

15.[价格范围动画输入](https://gitee.com/superstephen/learn-html-css-js/tree/master/15.PriceRangeSlider)

16.[RangeInputWithSvelteJSPlugin](https://gitee.com/superstephen/learn-html-css-js/tree/master/16.range-input-with-pips-svelte-js-plugin)

17.[代办清单](https://gitee.com/superstephen/learn-html-css-js/tree/master/17.ToDoList)

18.[磨砂玻璃卡片动画](https://gitee.com/superstephen/learn-html-css-js/tree/master/18.GlassmorphismCardAnimiation)

19.[个人介绍卡片设计](https://gitee.com/superstephen/learn-html-css-js/tree/master/19.CSSCardUIDesign)

20.[磨砂玻璃数字时钟](https://gitee.com/superstephen/learn-html-css-js/tree/master/20.GlassEffectDigitalWatch)

21.[LandingPage-图片三分屏入场动画](https://gitee.com/superstephen/learn-html-css-js/tree/master/21.PicUpDownSlider)

22.[下载按钮动画DownloadButtonAnimation](https://gitee.com/superstephen/learn-html-css-js/tree/master/22.DownloadButtonAnimation)

23.[密码强度检测PasswordStrengthChecker](https://gitee.com/superstephen/learn-html-css-js/tree/master/23.PasswordStrengthChecker)

24.[游戏排名动画MobileGamesRankingListUI](https://gitee.com/superstephen/learn-html-css-js/tree/master/24.MobileGamesRankingListUI)

25.[旋转星球切换动画](https://gitee.com/superstephen/learn-html-css-js/tree/master/25.planet)